﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteController : MonoBehaviour {

    public AudioClip[] audioClipArray;

    AudioSource audio;
    Rigidbody2D noteRgidbody;

    // ピアノの音のオーディオ番号
    int audioNumber = 0;

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
        noteRgidbody = GetComponent<Rigidbody2D>();
        //audio.clip = audioClipArray[audioNumber];
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        // バーに当たったら音を流す
        if (other.gameObject.tag == "Bar")
        {
            PlaySound();
        }
    }

    public void PlaySound()
    {
        //audio = GetComponent<AudioSource>();
        audio.clip = audioClipArray[audioNumber];
        audio.Play();
        Debug.Log("Play!");
    }

    public void destroyNote()
    {
        Destroy(gameObject);
    }
}
