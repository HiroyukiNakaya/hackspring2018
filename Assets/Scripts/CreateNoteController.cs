﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNoteController : MonoBehaviour
{
    // 生成したいPrefab
    public GameObject[] notePrefabArray;
    // クリックした位置座標
    private Vector3 clickPosition;
    // 音程の数字
    int intervalNumber;
    // 置いたところの座標のリスト
    List<Vector3> pastPositionList;
    // Use this for initialization
    void Start()
    {
        intervalNumber = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // マウス入力で左クリックをした瞬間
        if (Input.GetMouseButtonDown(0))
        {
            // ここでの注意点は座標の引数にVector2を渡すのではなく、Vector3を渡すことである。
            // Vector3でマウスがクリックした位置座標を取得する
            clickPosition = Input.mousePosition;
            // Z軸修正
            clickPosition.z = 10f;

            // 前にその場所に置いてないかを判定
            if (pastPositionList[0] != null)
            {
                for (int i = 0; i < pastPositionList.Count; i++)
                {
                    if (clickPosition == pastPositionList[i])
                    {
                        pastPositionList.Remove(pastPositionList[i]);
                        return;
                    }
                }
            }
            // オブジェクト生成 : オブジェクト(GameObject), 位置(Vector3), 角度(Quaternion)
            // ScreenToWorldPoint(位置(Vector3))：スクリーン座標をワールド座標に変換する
            Instantiate(notePrefabArray[intervalNumber], Camera.main.ScreenToWorldPoint(clickPosition), notePrefabArray[intervalNumber].transform.rotation);
            // 置いたらその座標を記憶
            pastPositionList.Add(clickPosition);
        }
    }
}

