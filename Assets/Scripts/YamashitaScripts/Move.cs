﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    public float speed;
    public float endPosition;

    Rigidbody2D rb2d;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start()
    {
        Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x >= endPosition) ScrollEnd();

        if(rb2d.velocity != Vector2.zero)
        {
            rb2d.velocity = new Vector2(speed + SliderController.velocity, 0);
        }
    }

    //棒を元の位置に戻す
    public void ScrollEnd()
    {
        transform.position = Vector2.zero;
    }


    //棒を動かす
    public void Play()
    {
        rb2d.velocity = new Vector2(speed, 0.0f);
    }

    //棒の動きを止める
    public void Stop()
    {
        rb2d.velocity = new Vector2(0, 0);
    }

    //棒を元の位置に戻して停止させる
    public void Back()
    {
        transform.position = Vector2.zero;
        rb2d.velocity = new Vector2(0, 0);
    }
}
