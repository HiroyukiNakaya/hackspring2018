﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderController : MonoBehaviour {

    public Slider mainSlider;

    public static float velocity;

	// Use this for initialization
	void Start () {
        mainSlider = GetComponent<Slider>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetVelocity()
    {
        velocity = mainSlider.value;
    }
}
